import { Database } from "./database.js";

const todoDB = new Database();


/*
 * Loads list of ToDos, calculates running tally, and expires deleted ToDos
 *
*/
function loadList() {
  let dataList = todoDB.loadDB();
  let todoList = document.createElement('div');
  todoList.setAttribute('id', 'list');
  let completed = document.createElement('div');
  completed.setAttribute('id', 'completed');
  document.getElementById('mainView').appendChild(todoList);
  document.getElementById('mainView').appendChild(completed);
  let tallyTotal = 0;
  let tallyCompleted = 0;
  for (let i = 0; i < dataList.length; i++) {
    let item = dataList[i];
    if (!item.pseudoUID) { // Skip database items without a pseudoUID
      continue
    }

    //console.log(item.todoLabel);
    if (isExpired(item)) {
      updateGrandTotal(item);
      console.log(item.pseudoUID + " expired, deleting...");
      todoDB.deleteItem(item.pseudoUID); // Delete expired items
      continue
    }
    tallyTotal++; // Increment totalTally after deleting expired tasks
    let listDiv = document.createElement('div');
    listDiv.setAttribute('class', 'todoList');
    listDiv.setAttribute('onclick', `viewEditToDo('${item.pseudoUID}')`);
    let listHtml = `
      <h3>${item.todoLabel}</h3>
      <p>${item.todoDesc}</p>
    `;
    listDiv.innerHTML = listHtml;
    if (item.completed) { // List completed items separately
      document.getElementById('completed').appendChild(listDiv);
      tallyCompleted++;
    } else {
      document.getElementById('list').appendChild(listDiv);
    }
  }
  // Print running tally
  document.getElementById('tally').innerHTML = tallyCompleted + "/" + tallyTotal;
  let grandTotal = JSON.parse(localStorage.getItem("completedList") || '[]').length; // Get completedList or assume empty
  document.getElementById('grandTotal').innerHTML = "Historical: " + grandTotal;
}

/*
 * Changes view and handles reloading database when switching to main view
 *
*/
function changeView(targetView) {
  let currentView = document.getElementsByClassName('currentView')[0];
  currentView.classList.remove('currentView'); // Removes class which identifies the current view
  currentView.style.display = "none"; // Disable current view
  
  let newView = document.getElementById(targetView);
  newView.classList.add('currentView'); // Adds currentView class to new view
  newView.style.display = "block"; // Enables new view
  
  if (document.getElementById('hamburgerMenuCheckbox').checked) {
    document.getElementById('hamburgerMenuCheckbox').checked = false; // Close menu if it's open
  }
  
  if (targetView == 'mainView') {
    document.getElementById('mainView').innerHTML = ""; // clear mainView
    loadList(); // Load list of ToDo's from the database
  }
}




/*
 * Creates or updates ToDo in the database
 *
*/
function saveToDo() {
  // Create hidden todoCreated input field if it doesn't exist
  if (!document.getElementById('todoCreated')) {
    let todoCreated = document.createElement("input");
    todoCreated.setAttribute("type", "hidden");
    todoCreated.setAttribute("name", "todoCreated");
    todoCreated.setAttribute("id", "todoCreated");
    todoCreated.setAttribute("value", Date.now());
    document.getElementById("todoForm").appendChild(todoCreated);
  }

  let completedDate = getCompletedDate();

  
  let todo = {
    "todoLabel": document.getElementById('todoLabel').value,
    "todoDesc": document.getElementById('todoDescription').value,
    "recurring": document.getElementById('todoRecurring').value,
    "created": document.getElementById('todoCreated').value,
    "completed": document.getElementById('todoCompleted').checked,
    "completedDate": completedDate
  };
  // Update if pseduoUID exists, otheriwse create new
  if (document.getElementById('pseudoUIDInput')) {
    todo.pseudoUID = document.getElementById('pseudoUIDInput').value;
    console.log("Updating ToDo: " + JSON.stringify(todo));
  } else {
    console.log("Creating ToDo: " + JSON.stringify(todo));
  }
  todoDB.createItem(todo);
  cleanForm();
  changeView('mainView');
}


/*
 * Creates a view with a populated form for updating an existing ToDo
 *
*/
function editToDoView(pseudoUID) {
  let dataList = todoDB.loadDB();
  let todoItem = dataList.find(o => o.pseudoUID === pseudoUID); // find item by pseudoUID
  // Add hidden pseudoUID to form
  let pseudoUIDElement = document.createElement("input");
  pseudoUIDElement.setAttribute('id', 'pseudoUIDInput');
  pseudoUIDElement.setAttribute('type', 'hidden');
  pseudoUIDElement.setAttribute('value', pseudoUID);
  document.getElementById('todoForm').appendChild(pseudoUIDElement);
  // Add hidden todoCompletedDate to form
  let todoCompletedDate = document.createElement("input");
  todoCompletedDate.setAttribute('id', 'todoCompletedDate');
  todoCompletedDate.setAttribute('type', 'hidden');
  todoCompletedDate.setAttribute('value', todoItem.completedDate);
  document.getElementById('todoForm').appendChild(todoCompletedDate);
  
  // Populate fields
  document.getElementById('todoLabel').value = todoItem.todoLabel;
  document.getElementById('todoDescription').value = todoItem.todoDesc;
  document.getElementById('todoRecurring').value = todoItem.recurring;
  document.getElementById('todoCompleted').checked = todoItem.completed;
  
  // Create delete button
  let deleteButton = document.createElement('button');
  deleteButton.setAttribute("id", "delete");
  deleteButton.setAttribute("onclick", "deleteToDo()");
  deleteButton.innerHTML = "Delete";
  document.getElementById('cancel').insertAdjacentElement('beforebegin', deleteButton);

  changeView('todoView');
}


/*
 * Deletes ToDo in current view and goes back to main screen
 *
*/
function deleteToDo() {
    let pseudoUID = document.getElementById('pseudoUIDInput').value;
    console.log("Deleting ToDo: " + pseudoUID);
    todoDB.deleteItem(pseudoUID);
    cleanForm()
    changeView('mainView');
}


/*
 * Cancel back to main screen
 *
*/
function cancel() {
  cleanForm()
  changeView('mainView');
}

/*
 * Clean the form inputs
 *
*/
function cleanForm() {
  document.getElementById('todoForm').reset(); // reset form
  if (document.getElementById("pseudoUIDInput")) {
    document.getElementById("pseudoUIDInput").remove(); // remove hidden psuedouid
  }
  if (document.getElementById("todoCompletedDate")) {
    document.getElementById("todoCompletedDate").remove(); // remove hidden todoCompletedDate
  }
  if (document.getElementById('delete')) {
    document.getElementById('delete').remove(); // remove delete button
  }
}

/*
 * Tests whether ToDo is expired
 *
*/
function isExpired(item) {
  if (!item.completed) { // ensure item is marked completed
    return false
  }
  let expireyWindow = (48 * 60 * 60 * 1000);
  return expireyWindow < (Date.now() - item.completedDate); // true if timestamp older than expireyWindow
}

/*
 * Returns the completedDate
 *
*/
function getCompletedDate() {
  // Determine todoCompletedDate
  let completedChecked = document.getElementById('todoCompleted').checked;
  if (document.querySelector('#todoCompletedDate')) {
    var completedDate = document.getElementById('todoCompletedDate').value;
  } else {
    var completedDate = null;
  }
  if (completedChecked && !isNaN(completedDate) && completedDate != null) { // checked and dated
    console.log("Use existing completed date..." + completedDate);
  } else if (completedChecked) { // checked and not dated
    completedDate = Date.now();
    console.log("Created new completed date..." + Date.now());
  } else { // not checked
    completedDate = null;
    console.log("Value wasn't a number or has been uncompleted.");
  }
  return completedDate
}

/*
 * Updates grand total value and completedList
 *
*/
function updateGrandTotal(item) {
  let record = [
    item.todoLabel,
    item.completedDate,
    item.pseudoUID
    ];
  console.log("Logging " + item.pseudoUID + " to completed list.");
  let completedList = JSON.parse(localStorage.getItem("completedList") || "[]");
  completedList.push(record);
  localStorage.setItem("completedList", JSON.stringify(completedList));
}

// Makes functions available globally
window.changeView = changeView;
window.cancel = cancel;
window.saveToDo = saveToDo;
window.deleteToDo = deleteToDo;
window.viewEditToDo = editToDoView;
window.exportDB = todoDB.exportDB;

// Load list for mainView after checkbox animation ends
document.getElementById('checkbox').addEventListener("animationend", function() {
  loadList();
});

