export class Database {
  constructor() {
    this.exportDB = this.exportDB.bind(this) // <- Add this
  }
  // loads localStorage as list of objects
  loadDB() {
    let newDB = [],
    keys = Object.keys(localStorage),
    i = keys.length;
    while ( i-- ) {
      console.log("Loading object: " + localStorage.getItem(keys[i]));
      newDB.push(JSON.parse(localStorage.getItem(keys[i])));
    }
    return newDB;
  }
  createItem(item) {
    if (!("pseudoUID" in item)) { // Create pseudoUID if this is a new item, otherwise update existing item
      let pseudoUID = '';
      let lengthUID = '20'
      while (pseudoUID.length < lengthUID) pseudoUID += Math.random().toString(36).substr(2, lengthUID - pseudoUID.length); // credit: https://stackoverflow.com/a/52464491
      console.log("Creating item with key: " + pseudoUID);
      item.pseudoUID = pseudoUID;
    } else {
        console.log("Updating item with key: " + item.pseudoUID);
    }
    localStorage.setItem(item.pseudoUID, JSON.stringify(item));
  }
  deleteItem(key) {
    localStorage.removeItem(key);
  }
  exportDB(elemID) {
    // https://stackoverflow.com/a/30800715
    // Associated HTML: <a id="exportData" style="display:none"></a>
    console.log("Exporting db with element: " + elemID);
    let downloadElement = document.getElementById(elemID)
    let database = this.loadDB();
    let stringifiedDatabase = "data:text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(database));
    downloadElement.setAttribute("href", stringifiedDatabase);
    downloadElement.setAttribute("download", document.title + ".json");
  }
  async importDB(file){
    //https://stackoverflow.com/a/36198572
    // Associated HTML: <a id="importData" style="display:none"></a>
    let response = await fetch(file);
    let importList = response.json();
    
  }
}


/*
 * Description: loads json file asynchronously
 * Args: file
*/
async function getJSON(file) {
  const response = await fetch(file);

  return response.json();
}


